
package inventory.model;


public abstract class Part {

    // Declare fields
    private int partId;
    private String name;
    private double price;
    private int inStock;
    private int min;
    private int max;
    
    // Constructor
    protected Part(int partId, String name, double price, int inStock, int min, int max) {
        this.partId = partId;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.min = min;
        this.max = max;
    }
    
    // Getters
    public int getPartId() {
        return partId;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getInStock() {
        return inStock;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
    
    // Setters
    public void setPartId(int partId) {
        this.partId = partId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }
    
    /**
     * Generate an error message for invalid values in a part
     * Valid part will return an empty string
     * @param name
     * @param price
     * @param inStock
     * @param min
     * @param max
     * @param errorMessage
     * @return 
     */
    public static String isValidPart(String name, String price, String inStock, String min, String max, String errorMessage) {
        if(name.equals("")) {
            errorMessage += "A name has not been entered. \n";
        }
        if (!price.matches("[-+]?[0-9]*\\.?[0-9]+") || !isInteger(inStock) || !isInteger(min) || !isInteger(max)){
            errorMessage += "The price, inventory, min and max needs to be a number\n";

        }
        else {
            if(Double.parseDouble(price) < 0.01) {
                errorMessage += "The price must be greater than 0.\n ";
            }

            if(Integer.parseInt(inStock) < 1) {
                errorMessage += "Inventory level must be greater than 0.\n ";
            }

            if(Integer.parseInt(min) > Integer.parseInt(max)) {
                errorMessage += "The Min value must be less than the Max value.\n ";
            }
            if(Integer.parseInt(inStock) < Integer.parseInt(min)) {
                errorMessage += "Inventory level is lower than minimum value.\n ";
            }
            if(Integer.parseInt(inStock) > Integer.parseInt(max)) {
                errorMessage += "Inventory level is higher than the maximum value. \n";
            }
        }
        return errorMessage;
    }

    @Override
    public String toString() {
        return this.partId+","+this.name+","+this.price+","+this.inStock+","+
                this.min+","+this.max;
    }

    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }
}
