package inventory.model;

import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.Test;

class InventoryTest {
    public InventoryRepository inventory = new InventoryRepository();



    @Test
    void isValidName() {
        assert inventory.lookupPart("Cog").getName().equals("Cog");
    }

    @Test
    void isValidId(){
        assert inventory.lookupPart("2").getPartId() == 2;
    }

    @Test
    void isValidNameAndId(){
        assert inventory.lookupPart("12").getName().equals("12a");
        assert inventory.lookupPart("12").getPartId() == 12;

    }

    @Test
    void isInvalidNothing(){
        assert inventory.lookupPart("blablabla") == null;
    }

    @Test
    void isInvalidNoData(){
        Inventory inventoryTest = new Inventory();
        assert inventoryTest.lookupPart("Cog") == null;
    }

    @Test
    void isNull()
    {
        assert inventory.lookupPart(null) == null;
    }



}