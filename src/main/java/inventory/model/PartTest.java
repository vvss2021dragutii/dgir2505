package inventory.model;

import org.junit.jupiter.api.*;

@DisplayName("TestingPart")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PartTest {

    @Test
    @Tag("ECP")
    @Order(1)
    void isValidPart_ECP_NameInvalid() {
        // act
        String errorMessage = Part.isValidPart("", "1.0", "2", "1","2","");

        //assert
        assert errorMessage.equals("A name has not been entered. \n");
    }

    @Test
    @Tag("ECP")
    @Order(2)
    void isValidPart_ECP_Valid(){
        String errorMessage = Part.isValidPart("Hello World", "1.0", "2", "1","2","");
        assert errorMessage.equals("");

    }

    @Test
    @Tag("ECP")
    @Order(3)
    @RepeatedTest(value = 5)
    void isValidPart_ECP_PriceInvalid() {
        // act
        String errorMessage = Part.isValidPart("Hello World", "1.p", "2", "1","2","");

        //assert
        assert errorMessage.equals("The price, inventory, min and max needs to be a number\n");
    }
    @Test
    @Tag("ECP")
    @Order(4)
    void isValidPart_ECP_minmaxInvalid() {
        // act
        String errorMessage = Part.isValidPart("Hello World", "1.0", "2", "2","1","");

        //assert
        assert errorMessage.equals("The Min value must be less than the Max value.\n Inventory level is higher than the maximum value. \n");
    }

    @Test
    @Tag("BVA")
    @Order(5)
    void isValidPart_BVA_Price_BelowLimit(){
        String errorMessage = Part.isValidPart("Hello World", "0.0", "2", "2","5","");
        assert errorMessage.equals("The price must be greater than 0.\n ");

    }

    @Test
    @Tag("BVA")
    @Order(6)
    void isValidPart_BVA_Price_Limit(){
        String errorMessage = Part.isValidPart("Hello World", "0.01", "2", "2","5","");
        assert errorMessage.equals("");

    }

    @Test
    @Tag("BVA")
    @Order(7)
    void isValidPart_BVA_Price_AboveLimit(){
        String errorMessage = Part.isValidPart("Hello World", "0.02", "2", "2","5","");
        assert errorMessage.equals("");

    }

    @Test
    @Tag("BVA")
    @Order(8)
    void isValidPart_BVA_inStock_BelowLimit(){
        String errorMessage = Part.isValidPart("Hello World", "1.0", "0", "0","5","");
        assert errorMessage.equals("Inventory level must be greater than 0.\n ");

    }

    @Test
    @Tag("BVA")
    @Order(9)
    void isValidPart_BVA_inStock_Limit(){
        String errorMessage = Part.isValidPart("Hello World", "1.0", "1", "1","5","");
        assert errorMessage.equals("");

    }

    @Test
    @Tag("BVA")
    @Order(10)
    void isValidPart_BVA_inStock_AboveLimit(){
        String errorMessage = Part.isValidPart("Hello World", "1.0", "2", "1","5","");
        assert errorMessage.equals("");

    }

    @Test
    @Disabled
    @Order(11)
    void isInteger() {
    }
}