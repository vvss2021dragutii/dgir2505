package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InventoryServiceTest {

    @Spy
    private Inventory inventory;

    @InjectMocks
    private InventoryRepository inventoryRepository;

    private InventoryService inventoryService;

    @BeforeAll
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        inventoryService= new InventoryService(inventoryRepository);
    }

    @Test
    public void lookupPart_PartExists(){
        Part part = new InhousePart(1,"a",2.0,2,1,5,2);
        String search = "a";
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList(part));
        assert inventoryService.lookupPart(search).equals(part);
    }


    @Test
    public void lookupPart_PartDoesNotExist(){
        Part part = new InhousePart(1,"a",2.0,2,1,5,2);
        String search = "a";
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList());
        assert inventoryService.lookupPart(search) == null;
    }

}