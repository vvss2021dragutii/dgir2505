package inventory;

import inventory.controller.MainScreenController;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;


public class Main extends Application {
    static final Logger logger = Logger.getLogger(Main.class);


    @Override
    public void start(Stage stage) throws Exception {
        BasicConfigurator.configure();

        InventoryRepository repo= new InventoryRepository();
        InventoryService service = new InventoryService(repo);
        logger.info(service.getAllProducts());
        logger.info(service.getAllParts());
        FXMLLoader loader= new FXMLLoader(getClass().getResource("/fxml/MainScreen.fxml"));

        Parent root=loader.load();
        MainScreenController ctrl=loader.getController();
        ctrl.setService(service);

        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
