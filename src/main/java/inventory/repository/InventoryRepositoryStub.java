package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;

public class InventoryRepositoryStub {
    private Inventory inventory;

    public InventoryRepositoryStub(){
        this.inventory=new Inventory();

    }
    public Part lookupPart (String search){
        return new InhousePart(1,"a",2.0,2,1,5,2);

    }
}
