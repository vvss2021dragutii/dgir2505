package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InventoryRepositoryTest {
    @Spy
    private Inventory inventory;

    @InjectMocks
    private InventoryRepository inventoryRepository;

    @BeforeAll
    public void setUp() {
        MockitoAnnotations.initMocks(this);
}


    @Test
    public void lookupPart_partExists(){
        Part part = new InhousePart(1,"a",2.0,2,1,5,2);
        String search = "a";
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList(part));
        assert inventoryRepository.lookupPart(search).equals(part);
    }

    @Test
    public void lookupPart_partDoesNotExists(){
        Part part1 = new InhousePart(1,"a",2.0,2,1,5,2);
        Part part2 = new OutsourcedPart(2,"b",3.0,2,1,5,"BlahBlahCar");

        String search = "a";
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList(part1));
        assert !inventoryRepository.lookupPart(search).equals(part2);
    }


}